@extends('master')

@section('konten')
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link " aria-current="page" href="home" style="color:black">HOME</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="daftar" style="color:black">DAFTAR</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="pengumuman" >PENGUMUMAN</a>
  </li>
</ul>
@endsection