@extends('master')

@section('konten')
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="home">HOME</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="daftar" style="color:black">DAFTAR</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="pengumuman" style="color:black">PENGUMUMAN</a>
  </li>
</ul>
@endsection