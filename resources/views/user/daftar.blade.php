@extends('master')

@section('konten')
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link " aria-current="page" href="home" style="color:black">HOME</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="daftar" >DAFTAR</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="pengumuman" style="color:black">PENGUMUMAN</a>
  </li>
</ul>
<div class="container"><br>
        <div class="col-md-6 col-md-offset-3">
            <h2 class="text-center">FORM PENDAFTARAN CALON RAPALA</h3>
            <hr>
            @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            @endif
            <form action="{{route('daftarcalon')}}" method="post">
            @csrf
                <div class="form-group">
                    <label><i class="fa fa-user"></i> Nama Lengkap</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required="">
                </div>
                <div class="form-group ">
                    <label><i class="fa fa-venus-mars" aria-hidden="true"></i> Jenis Kelamin</label>
                    <div class="form-check form-check-inline ms-3">
                      <input class="form-check-input" type="radio" name="jeniskelamin" id="inlineRadio1" value="Laki-Laki">
                      <label class="form-check-label" for="inlineRadio1">Laki-Laki</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="jeniskelamin" id="inlineRadio2" value="Perempuan">
                      <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                    </div>
                </div>
                <div class="form-group">
                    <label><i class="fa fa-id-card"></i> Nomor Induk Kependudukan</label>
                    <input type="text" name="nik" class="form-control" placeholder="Nomor Induk Kependudukan" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-location-arrow"></i> Tempat Lahir</label>
                    <input type="text" name="tempatlahir" class="form-control" placeholder="Tempat Lahir" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-calendar"></i> Tanggal Lahir</label>
                    <input type="date" name="tanggallahir" class="form-control" placeholder="Tanggal Lahir" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-address-book"></i> Alamat</label>
                    <input type="text" name="alamat" class="form-control" placeholder="Alamat" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-comment"></i> No Hp</label>
                    <input type="text" name="nohp" class="form-control" placeholder="No Hp" required="">
                </div>
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-user"></i> DAFTAR</button>
                <hr>
                
            </form>
        </div>
    </div>
@endsection