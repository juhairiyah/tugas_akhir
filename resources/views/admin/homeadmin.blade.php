@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link bg-secondary text-white shadow p-3 mb-5 rounded" aria-current="page" href="home" style="">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="perankingan" style="color:black">PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div class="bg-secondary text-white shadow p-3 mb-5 rounded" style="width:100%">
      <table class="table table-lg">
      <thead>
        <tr class="text-white">
          <th scope="col">#</th>
          <th scope="col">NAMA LENGKAP</th>
          <th scope="col">JENIS KELAMIN</th>
          <th scope="col">NOMOR INDUK KEPENDUDUKAN</th>
          <th scope="col">TEMPAT LAHIR</th>
          <th scope="col">TANGGAL LAHIR</th>
          <th scope="col">ALAMAT</th>
          <th scope="col">NO HP</th>
        </tr>
      </thead>
      @foreach ($tambah as $item)
            <tr class="text-white">
                <td>{{$item->id}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->jeniskelamin}}</td>
                <td>{{$item->nik}}</td>
                <td>{{$item->tempatlahir}}</td>
                <td>{{$item->tanggallahir}}</td>
                <td>{{$item->alamat}}</td>
                <td>{{$item->nohp}}</td>
            </tr>
        @endforeach
    </table>
  </div>
</div>
@endsection