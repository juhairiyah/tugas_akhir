@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" aria-current="page" href="home" style="color:black">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active bg-secondary text-white shadow p-3 mb-5 rounded" href="perankingan" >PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div style="width:100%">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="perankingan">ASPEK PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="kriteriapenilaian" style="color:black">KRITERIA PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="profilematching" style="color:black">PROFILE MATCHING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('hasilperhitungan')}}" style="color:black">HASIL PERHITUNGAN</a>
      </li>
    </ul>
    <a class="btn btn-secondary mt-4 shadow p-2" style="width:100px;height:30px" href="tambahaspek">TAMBAH</a>
    <div class="bg-secondary text-white shadow p-3 mb-5 mt-2 rounded">
      <table class="table table-lg">
        <thead>
          <tr class="text-white">
            <th scope="col">#</th>
            <th scope="col">ASPEK PENILAIAN</th>
            <th scope="col">PRESENTASE</th>
            <th scope="col">CORE FACTOR</th>
            <th scope="col">SECONDARY FACTOR</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>
        @foreach ($aspek as $item)
            <tr class="text-white">
                <td>{{$item->id}}</td>
                <td>{{$item->aspekpenilaian}}</td>
                <td>{{$item->presentase}}</td>
                <td>{{$item->corefactor}}</td>
                <td>{{$item->secondaryfactor}}</td>
                <td>
                    <a href="{{route ('editaspek', $item->id)}}"><button class="btn btn-primary">edit</button></a>
                    <a href="{{route ('hapusaspek', $item->id)}}"><button class="btn btn-danger">hapus</button></a>
                </td>

            </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection