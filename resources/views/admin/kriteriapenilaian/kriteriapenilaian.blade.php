@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" aria-current="page" href="home" style="color:black">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active bg-secondary text-white shadow p-3 mb-5 rounded" href="perankingan" >PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div style="width:100%">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link " aria-current="page" href="perankingan" style="color:black">ASPEK PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="kriteriapenilaian" >KRITERIA PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="profilematching" style="color:black">PROFILE MATCHING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="hasilperhitungan" style="color:black">HASIL PERHITUNGAN</a>
      </li>
    </ul>
    <a class="btn btn-secondary mt-4 shadow p-2" style="width:100px;height:30px" href="tambahkriteria">TAMBAH</a>
    <div class="bg-secondary text-white shadow p-3 mb-5 mt-2 rounded">
      <table class="table table-lg">
        <thead>
          <tr class="text-white">
            <th scope="col">No</th>
            <th scope="col">ASPEK PENILAIAN</th>
            <th scope="col">KRITERIA</th>
            <th scope="col">TARGET</th>
            <th scope="col">TIPE</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>
        @foreach ($kriteria as $item)
            <tr class="text-white">
                <td>{{$loop->iteration}}</td>
                <td>{{$item->aspek_id}}</td>
                <td>{{$item->kriteria}}</td>
                <td>{{$item->target}}</td>
                <td>{{$item->tipe}}</td>
                <td>
                    <a href="{{route('editkriteria', $item->id)}}"><button class="btn btn-primary">edit</button></a>
                    <a href="{{route('hapuskriteria', $item->id)}}"><button class="btn btn-danger">hapus</button></a>
                </td>

            </tr>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection