@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" aria-current="page" href="home" style="color:black">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active bg-secondary text-white shadow p-3 mb-5 rounded" href="perankingan" >PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div style="width:100%">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link " aria-current="page" href="perankingan" style="color:black">ASPEK PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="kriteriapenilaian" >KRITERIA PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="profilematching" style="color:black">PROFILE MATCHING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="hasilperhitungan" style="color:black">HASIL PERHITUNGAN</a>
      </li>
    </ul>
    <div class="container"><br>
        <div class="col-md-6 col-md-offset-3">
            <h2 class="text-center">TAMBAH KRITERIA PENILAIAN</h3>
            <hr>
            @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            @endif
            <form method="post" action="{{ route('tambahkriteria1') }}">
            @csrf
                <div class="form-group">
                    <label><i class="fa fa-user"></i> Aspek Penilaian</label>
                    <select class="form-select" name="aspek_id" id="aspek_id">
                        @foreach ($aspek as $item)
                            <option value="{{$item->id}}">{{$item->aspekpenilaian}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label><i class="fa fa-id-card"></i> Kriteria</label>
                    <input type="text" name="kriteria" class="form-control" placeholder="Presentase" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-location-arrow"></i> Target</label>
                    <select class="form-select" name="target" id="aspek_id">
                            <option value="1">Sangat Kurang</option>
                            <option value="2">Kurang</option>
                            <option value="3">Cukup</option>
                            <option value="4">Baik</option>
                            <option value="5">Sangat Baik</option>
                    </select>
                </div>
                <div class="form-group">
                    <label><i class="fa fa-calendar"></i> Tipe</label>
                    <select class="form-select" name="tipe" id="aspek_id">
                            <option value="Core">Core Factor</option>
                            <option value="Secondary">Secondary Factor</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-user"></i> TAMBAH</button>
                <hr>
                
            </form>
        </div>
    </div>
  </div>
</div>
@endsection