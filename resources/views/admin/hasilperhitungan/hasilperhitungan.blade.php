@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" aria-current="page" href="home" style="color:black">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active bg-secondary text-white shadow p-3 mb-5 rounded" href="perankingan" >PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div style="width:100%">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link " aria-current="page" href="perankingan" style="color:black">ASPEK PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="kriteriapenilaian" style="color:black">KRITERIA PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="profilematching" style="color:black">PROFILE MATCHING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="hasilperhitungan">HASIL PERHITUNGAN</a>
      </li>
    </ul>
    <div class="bg-secondary text-white shadow p-3 mb-5 mt-2 rounded">
      <!-- hasilperhitungan.blade.php -->

    @foreach ($data as $aspek => $nilai_nama)
        <h3>Aspek {{ $aspek }}</h3>
        <table class="table table-lg">
        <thead>
            <tr class="text-white">
                <th>Nama</th>
                @foreach($nilai_nama[key($nilai_nama)] as $k => $n)
                <th class="text-center" style="word-wrap: break-word">{{ $k }}</th>
                @endforeach
            </tr>
            
        </thead>
        <tbody>
            @foreach($nilai_nama as $nama => $nilai)
                <tr class="text-white">
                    <td>{{ $nama }}</td>
                    @foreach($nilai as $n)
                        <td class="text-center" style="word-wrap: break-word">{{ $n['nilai'] }}</td>
                    @endforeach
                </tr>
                @endforeach
                <tr class="text-white">
                  <th>Target</th>
                  @foreach($nilai as $n)
                    <th class="text-center" style="word-wrap: break-word">{{ $n['target'] }}</th>
                    @endforeach
                </tr>
        </tbody>
        </table>
    @endforeach
    <h2 class="mt-5 mb-3">#MENGHITUNG GAP</h2>
    @foreach ($data as $aspek => $nilai_nama)
        <h3>Aspek {{ $aspek }}</h3>
        <table class="table table-lg">
        <thead>
            <tr class="text-white">
                <th>Nama</th>
                @foreach($nilai_nama[key($nilai_nama)] as $k => $n)
                <th class="text-center" style="word-wrap: break-word">{{ $k }}</th>
                @endforeach
            </tr>
            
        </thead>
        <tbody>
            @foreach($nilai_nama as $nama => $nilai)
                <tr class="text-white">
                    <td>{{ $nama }}</td>
                    @foreach($nilai as $n)
                    @php
                    $selisih = $n['nilai'] - $n['target'];
                    @endphp
                        <td class="text-center" style="word-wrap: break-word">{{ $selisih }}</td>
                    @endforeach
                </tr>
                @endforeach
        </tbody>
        </table>
    @endforeach
    <h2 class="mt-5 mb-3">#PEMBOBOTAN</h2>
    @foreach ($data as $aspek => $nilai_nama)
        <h3>Aspek {{ $aspek }}</h3>
        <table class="table table-lg">
        <thead>
            <tr class="text-white">
                <th>Nama</th>
                @foreach($nilai_nama[key($nilai_nama)] as $k => $n)
                <th class="text-center" style="word-wrap: break-word">{{ $k }}</th>
                @endforeach
            </tr>
            
        </thead>
        <tbody>
            @foreach($nilai_nama as $nama => $nilai)
                <tr class="text-white">
                    <td>{{ $nama }}</td>
                    @foreach($nilai as $n)
                    @php
                    $selisih = $n['nilai'] - $n['target'];
                    if($selisih == 0) {
                      $selisih = 5.0;
                    } elseif($selisih == 1) {
                      $selisih = 4.5;
                    } elseif($selisih == -1) {
                      $selisih = 4.0;
                    } elseif($selisih == 2) {
                      $selisih = 3.5;
                    } elseif($selisih == -2) {
                      $selisih = 3.0;
                    } elseif($selisih == 3) {
                      $selisih = 2.5;
                    } elseif($selisih == -3) {
                      $selisih = 2.0;
                    } elseif($selisih == 4) {
                      $selisih = 1.5;
                    } elseif($selisih == -4) {
                      $selisih = 1.0;
                    }
                    @endphp
                        <td class="text-center" style="word-wrap: break-word">{{ $selisih }}</td>
                    @endforeach
                </tr>
                @endforeach
        </tbody>
        </table>
    @endforeach


    </div>

  </div>
</div>
@endsection