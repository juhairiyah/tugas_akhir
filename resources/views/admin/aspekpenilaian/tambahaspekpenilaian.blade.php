@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" aria-current="page" href="home" style="color:black">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active bg-secondary text-white shadow p-3 mb-5 rounded" href="perankingan" >PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div style="width:100%">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="perankingan">ASPEK PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="kriteriapenilaian" style="color:black">KRITERIA PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="profilematching" style="color:black">PROFILE MATCHING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="hasilperhitungan" style="color:black">HASIL PERHITUNGAN</a>
      </li>
    </ul>
    <div class="container"><br>
        <div class="col-md-6 col-md-offset-3">
            <h2 class="text-center">TAMBAH ASPEK PENILAIAN</h3>
            <hr>
            @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            @endif
            <form method="post" action="{{ route('tambahaspek1') }}">
            @csrf
                <div class="form-group">
                    <label><i class="fa fa-user"></i> Aspek Penilaian</label>
                    <input type="text" name="aspekpenilaian" class="form-control" placeholder="Aspek Penilaian" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-id-card"></i> Presentase</label>
                    <input type="text" name="presentase" class="form-control" placeholder="Presentase" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-location-arrow"></i> Core Factor</label>
                    <input type="text" name="corefactor" class="form-control" placeholder="Core Factor" required="">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-calendar"></i> Secondary Factor</label>
                    <input type="text" name="secondaryfactor" class="form-control" placeholder="Secondary Factor" required="">
                </div>
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-user"></i> TAMBAH</button>
                <hr>
                
            </form>
        </div>
    </div>
  </div>
</div>
@endsection