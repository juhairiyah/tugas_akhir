@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" aria-current="page" href="home" style="color:black">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active bg-secondary text-white shadow p-3 mb-5 rounded" href="perankingan" >PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div style="width:100%">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link " aria-current="page" href="perankingan" style="color:black">ASPEK PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="kriteriapenilaian" style="color:black">KRITERIA PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="profilematching" >PROFILE MATCHING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="hasilperhitungan" style="color:black">HASIL PERHITUNGAN</a>
      </li>
    </ul>

    <div class="dropdown mt-3">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Pilih Aspek
        </button>
        <ul class="dropdown-menu">
            @foreach ($aspek as $item)
                <li><a class="dropdown-item" href="{{route('jenisaspek',['id'=>$item->id])}}">{{$item->aspekpenilaian}}</a></li>
            @endforeach
            
        </ul>
    </div>


    <div class="bg-secondary text-white shadow p-3 mb-5 mt-2 rounded">
  
    </div>
  </div>
</div>
@endsection