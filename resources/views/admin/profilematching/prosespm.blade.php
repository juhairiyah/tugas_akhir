@extends('master')

@section('konten')
<div class="d-flex align-items-start">
  <div style="width:200px">
    <ul class="nav flex-column me-5">
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" aria-current="page" href="home" style="color:black">LIST DATA</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active bg-secondary text-white shadow p-3 mb-5 rounded" href="perankingan" >PERANKINGAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link shadow p-3 mb-5 rounded" href="pengumumanadmin" style="color:black">PENGUMUMAN ADMIN</a>
      </li>
    </ul>
  </div>
  <div style="width:100%">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link " aria-current="page" href="perankingan" style="color:black">ASPEK PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="kriteriapenilaian" style="color:black">KRITERIA PENILAIAN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="profilematching" >PROFILE MATCHING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="hasilperhitungan" style="color:black">HASIL PERHITUNGAN</a>
      </li>
    </ul>

    <div class="dropdown mt-3">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
           {{$jenis->aspekpenilaian}}
        </button>
        <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="{{route('profilematching')}}">Pilih Aspek</a></li>
            @foreach ($aspek as $item)
                <li><a class="dropdown-item" href="{{route('jenisaspek',['id'=>$item->id])}}">{{$item->aspekpenilaian}}</a></li>
            @endforeach
            
        </ul>
    </div>


    <div class="bg-secondary text-white shadow p-3 mb-5 mt-2 rounded">
    @if (Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
    @endif
    <form action="{{route('simpandata')}}" method="POST">
    @csrf
    <table class="table table-lg">
        <thead>
        <tr class="text-white">
            <th scope="col">No</th>
            <th scope="col">NAMA</th>
            @foreach ($pm as $item)
            <th scope="col">{{ $item->kriteria }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach ($nama as $index => $item)
            <tr>
            <td class="text-white">{{ $loop->iteration }}</td>
            <td class="text-white">{{ $item->nama }}</td>
            @foreach ($pm as $kriteria)
                <td>
                <select class="form-select" name="nilai_{{ $kriteria->id }}_{{ $item->id }}" id="nilai_{{ $kriteria->id }}_{{ $item->id }}">
                    <option value="1">Sangat Kurang</option>
                    <option value="2">Kurang</option>
                    <option value="3">Cukup</option>
                    <option value="4">Baik</option>
                    <option value="5">Sangat Baik</option>
                </select>
                </td>
            @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
    <button type="submit" class="btn btn-primary">Simpan</button>
    </form>

    </div>
  </div>
</div>
@endsection