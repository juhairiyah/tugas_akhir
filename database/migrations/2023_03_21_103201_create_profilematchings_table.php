<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilematchingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profilematchings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nama_id');
            $table->unsignedBigInteger('kriteria_id');
            $table->string('nilai');
            $table->foreign('nama_id')->references('id')->on('datarapalas')->onupdate('cascade')->onDelete('cascade');
            $table->foreign('kriteria_id')->references('id')->on('kriteriapenilaians')->onupdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profilematchings');
    }
}
