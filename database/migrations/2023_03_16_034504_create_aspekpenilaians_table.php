<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAspekpenilaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspekpenilaians', function (Blueprint $table) {
            $table->id();
            $table->string('aspekpenilaian');
            $table->string('presentase');
            $table->string('corefactor');
            $table->string('secondaryfactor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspekpenilaians');
    }
}
