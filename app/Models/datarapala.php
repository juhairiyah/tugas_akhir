<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class datarapala extends Model
{
    protected $table = "datarapalas";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama','jeniskelamin','nik','tempatlahir','tanggallahir','alamat','nohp'
    ];

    public function profilematching(){
        return $this->belongsToMany(profilematching::class, 'nama_id','id');
    }

}
