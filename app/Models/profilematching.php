<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profilematching extends Model
{
    protected $table = "profilematchings";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama_id','kriteria_id','nilai'
    ];

    public function kriteriapenilaian(){
        return $this->belongsToMany(kriteriapenilaian::class);
    }
    public function datarapala(){
        return $this->belongsToMany(datarapala::class);
    }
}
