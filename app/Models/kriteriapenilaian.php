<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kriteriapenilaian extends Model
{
    protected $table = "kriteriapenilaians";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','aspek_id','kriteria','target','tipe'
    ];

    public function aspekpenilaian(){
        return $this->belongsTo('App\Models\aspekpenilaian');
    }

    public function profilematching(){
        return $this->belongsToMany(profilematching::class, 'kriteria_id','id');
    }
    
}
