<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\datarapala;
use App\Models\aspekpenilaian;
use App\Models\kriteriapenilaian;
use App\Models\profilematching;
use Session;
  
class HomeController extends Controller
{
    public function index(Request $request) {
        $user = $request->User();

        if($user->role('Administrator')) {
            $tambah=datarapala::all();
            return view('admin/homeadmin', compact('tambah'));
        }
        elseif($user->role('User')) {
           return view('user/homeuser');
        }
    }
    public function daftar()
    {
        return view('user/daftar');
    }
    public function pengumuman()
    {
        return view('user/pengumuman');
    }
    public function perankingan()
    {
        $aspek=aspekpenilaian::all();
        return view('admin/perankingan', compact('aspek'));
    }
    public function pengumumanadmin()
    {
        return view('admin/pengumumanadmin');
    }
    public function tambahaspek()
    {
        return view('admin/aspekpenilaian/tambahaspekpenilaian');
    }
    public function tambahkriteria()
    {
        $aspek=aspekpenilaian::all();
        return view('admin/kriteriapenilaian/tambahkriteriapenilaian', compact('aspek'));
    }
    public function kriteriapenilaian()
    {
        $kriteria=kriteriapenilaian::all();
        return view('admin/kriteriapenilaian/kriteriapenilaian', compact('kriteria'));
    }
    public function daftarcalon(Request $request)
    {
        datarapala::create([
            'nama' => $request-> nama,
            'jeniskelamin' => $request-> jeniskelamin,
            'nik' => $request-> nik,
            'tempatlahir' => $request-> tempatlahir,
            'tanggallahir' => $request-> tanggallahir,
            'alamat' => $request-> alamat,
            'nohp' => $request-> nohp,
        ]);
        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('daftarrapala');
    }
    public function daftarrapala()
    {
        return view('user/daftar');
    }
    public function tambahaspek1(Request $request)
    {
        aspekpenilaian::create([
            'aspekpenilaian' => $request-> aspekpenilaian,
            'presentase' => $request-> presentase,
            'corefactor' => $request-> corefactor,
            'secondaryfactor' => $request-> secondaryfactor,
        ]);
        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('tambahaspek2');
    }
    public function tambahkriteria1(Request $request)
    {
        kriteriapenilaian::create([
            'aspek_id' => $request-> aspek_id,
            'kriteria' => $request-> kriteria,
            'target' => $request-> target,
            'tipe' => $request-> tipe,
        ]);
        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('tambahkriteria2');
    }
    public function tambahaspek2()
    {
        return view('admin/aspekpenilaian/tambahaspekpenilaian');
    }
    public function tambahkriteria2()
    {
        $aspek=aspekpenilaian::all();
        return view('admin/kriteriapenilaian/tambahkriteriapenilaian', compact('aspek'));
    }

    public function editaspek($id)
    {
        $aspek = aspekpenilaian::find($id);   
        // dd($aspek);
        return view('Admin.aspekpenilaian.editaspekpenilaian', compact('aspek'));
    }
    public function simpaneditaspek(Request $request, $id)
    {
        $aspek = aspekpenilaian::find($id);
        $save = $aspek->update([
            'aspekpenilaian'=>$request->aspekpenilaian,
            'presentase'=>$request->presentase,
            'corefactor'=>$request->corefactor,
            'secondaryfactor'=>$request->secondaryfactor,
        ]);
        if ($save) {
            $aspek = aspekpenilaian::where('id')->first();
            return redirect()->route('perankingan');
        }
    }

    public function hapusaspek($id)
    {
        $aspek = aspekpenilaian::findorfail($id);
        $aspek->delete();
        return back();
    }
    
    public function editkriteria($id)
    {
        $aspek = aspekpenilaian::all();
        $kriteria = kriteriapenilaian::find($id);
        // dd($kriteria);
        // dd($aspek);
        return view('Admin.kriteriapenilaian.editkriteriapenilaian', compact('aspek','kriteria'));
    }

    public function simpaneditkriteria(Request $request, $id)
    {
        $kriteria = kriteriapenilaian::find($id);
        $save = $kriteria->update([
            'aspek_id' => $request-> aspek_id,
            'kriteria' => $request-> kriteria,
            'target' => $request-> target,
            'tipe' => $request-> tipe,
        ]);
        if ($save) {
            $kriteria = kriteriapenilaian::where('id')->first();
            return redirect()->route('kriteriapenilaian');
        }
    }
    
    public function hapuskriteria($id)
    {
        $kriteria = kriteriapenilaian::findorfail($id);
        $kriteria->delete();
        return back();
    }

    public function profilematching()
    {
        $aspek = aspekpenilaian::all();
        return view('Admin.profilematching.profilematching', compact('aspek'));
    }

    public function jenisaspek($id)
    {
        $aspek = aspekpenilaian::all();
        $nama = datarapala::all();
        $jenis = aspekpenilaian::find($id);
        $pm = kriteriapenilaian::where('aspek_id',$jenis->id)->get();
        // dd($pm);
        return view('Admin.profilematching.prosespm', compact('nama','jenis','pm','aspek'));
    }

    public function simpandata(Request $request)
    {
        // looping untuk mendapatkan id kriteria dan id nama mahasiswa
        foreach ($request->except('_token') as $key => $value) {
            // memisahkan id kriteria dan id nama mahasiswa
            $keys = explode('_', $key);
            $kriteria_id = $keys[1];
            $nama_id = $keys[2];
            // menyimpan data ke database
            $data = [
                'kriteria_id' => $kriteria_id,
                'nama_id' => $nama_id,
                'nilai' => $value,
            ];
            profilematching::create($data);
        }
        // redirect ke halaman index
        // dd($data);
        
        // Set pesan notifikasi ke session flash
        Session::flash('success', 'Data berhasil disimpan');
        return redirect()->back();

    }


    public function hasilperhitungan()
    {
        $nama = datarapala::all();
        $pm = profilematching::all();
        $aspeks = aspekpenilaian::all();
        $data = [];
    
        foreach ($aspeks as $aspek) {
            $kriterias = kriteriapenilaian::where('aspek_id', $aspek->id)->get();
    
            foreach ($nama as $n) {
                $nilai = [];
                foreach ($kriterias as $k) {
                    $nilai[$k->kriteria] = [
                        'nilai' => $pm
                            ->where('nama_id', $n->id)
                            ->where('kriteria_id', $k->id)
                            ->first()
                            ->nilai,
                        'target' => $k->target,
                    ];
                }
                $data[$aspek->aspekpenilaian][$n->nama] = $nilai;
            }
        }
    
        // dd($data);
        return view('Admin.hasilperhitungan.hasilperhitungan', compact('data','kriterias','aspeks'));
    }
    



}