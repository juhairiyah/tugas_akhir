<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('daftar', [HomeController::class, 'daftar'])->name('daftar')->middleware('auth');
Route::get('pengumuman', [HomeController::class, 'pengumuman'])->name('pengumuman')->middleware('auth');
Route::get('perankingan', [HomeController::class, 'perankingan'])->name('perankingan')->middleware('auth');
Route::get('tambahaspek', [HomeController::class, 'tambahaspek'])->name('tambahaspek')->middleware('auth');
Route::get('editaspek/{id}', [HomeController::class, 'editaspek'])->name('editaspek')->middleware('auth');
Route::put('simpaneditaspek/{id}', [HomeController::class, 'simpaneditaspek'])->name('simpaneditaspek')->middleware('auth');
Route::get('hapusaspek/{id}', [HomeController::class, 'hapusaspek'])->name('hapusaspek')->middleware('auth');
Route::get('tambahkriteria', [HomeController::class, 'tambahkriteria'])->name('tambahkriteria')->middleware('auth');
Route::get('editkriteria/{id}', [HomeController::class, 'editkriteria'])->name('editkriteria')->middleware('auth');
Route::put('simpaneditkriteria/{id}', [HomeController::class, 'simpaneditkriteria'])->name('simpaneditkriteria')->middleware('auth');
Route::get('hapuskriteria/{id}', [HomeController::class, 'hapuskriteria'])->name('hapuskriteria')->middleware('auth');
Route::get('pengumumanadmin', [HomeController::class, 'pengumumanadmin'])->name('pengumumanadmin')->middleware('auth');
Route::post('daftarcalon', [HomeController::class, 'daftarcalon'])->name('daftarcalon')->middleware('auth');
Route::post('tambahaspek1', [HomeController::class, 'tambahaspek1'])->name('tambahaspek1')->middleware('auth');
Route::post('tambahkriteria1', [HomeController::class, 'tambahkriteria1'])->name('tambahkriteria1')->middleware('auth');
Route::get('daftarrapala', [HomeController::class, 'daftarrapala'])->name('daftarrapala')->middleware('auth');
Route::get('tambahaspek2', [HomeController::class, 'tambahaspek2'])->name('tambahaspek2')->middleware('auth');
Route::get('tambahkriteria2', [HomeController::class, 'tambahkriteria2'])->name('tambahkriteria2')->middleware('auth');
Route::get('kriteriapenilaian', [HomeController::class, 'kriteriapenilaian'])->name('kriteriapenilaian')->middleware('auth');
Route::get('profilematching', [HomeController::class, 'profilematching'])->name('profilematching')->middleware('auth');
Route::get('jenisaspek/{id}', [HomeController::class, 'jenisaspek'])->name('jenisaspek')->middleware('auth');
Route::post('simpandata', [HomeController::class, 'simpandata'])->name('simpandata')->middleware('auth');
Route::get('hasilperhitungan', [HomeController::class, 'hasilperhitungan'])->name('hasilperhitungan')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');

//REGISTER
Route::get('register', [RegisterController::class, 'register'])->name('register');
Route::post('register/action', [RegisterController::class, 'actionregister'])->name('actionregister');